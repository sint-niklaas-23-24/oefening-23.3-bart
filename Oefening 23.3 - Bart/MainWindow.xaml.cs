﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_23._3___Bart
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Werknemer mijnWerknemer;
        List<Werknemer> lijstWerknemers = new List<Werknemer>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (! (String.IsNullOrEmpty(txtAchternaam.Text) && String.IsNullOrEmpty(txtVoornaam.Text)))
                {
                    if (txtVerdiensten.Text.Any(Char.IsDigit))
                    {
                        mijnWerknemer = new Werknemer(txtAchternaam.Text, txtVoornaam.Text, Convert.ToDouble(txtVerdiensten.Text));
                        lijstWerknemers.Add(mijnWerknemer);
                        lstResultaat.Items.Add(mijnWerknemer.VolledigeWeergave.ToString());
                        
                        TXTLeegmaken();
                    }
                    else
                    {
                        MessageBox.Show("Gelieve een cijfer in te vullen bij Verdiensten aub.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Gelieve de naam en voornaam in te vullen aub.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch
            {
                MessageBox.Show("Er is een fout opgetreden bij het toevoegen van de gegevens.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            
        }
        private void TXTLeegmaken()
        {
            txtAchternaam.Text = string.Empty;
            txtVoornaam.Text = string.Empty;
            txtVerdiensten.Text = string.Empty;
        }
    }
}