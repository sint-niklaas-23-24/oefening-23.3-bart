﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_23._3___Bart
{
    class Werknemer
    {
        private string _naam;
        private double _verdiensten;
        private string _voornaam;

        public Werknemer() { }
        public Werknemer (string naam, string voornaam, double verdiensten)
        {
            _naam = naam;
            _voornaam = voornaam;
            _verdiensten = verdiensten;
        }

        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }

        public double Verdiensten
        {
            get { return _verdiensten; }
            set { _verdiensten = value; }
        }

        public string Voornaam
        {
            get { return _voornaam; }
            set { _voornaam = value; }
        }

        public string VolledigeWeergave
        {
            get { return Voornaam.ToString() + Naam.ToString().PadLeft(25) + "€".PadLeft(25) + Verdiensten.ToString("0.00").Replace(".", ",").PadLeft(8) + Environment.NewLine; }
        }

        }
}
